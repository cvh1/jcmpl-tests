package io.solvice.jcmpl.test;

import jCMPL.*;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DietTest {
    @Test
    public void run() throws CmplException {
        Cmpl model = new Cmpl("target/test-classes/diet.cmpl");
        CmplSet nutr = new CmplSet("NUTR");
        String[] nutrLst = {"A", "B1", "B2", "C"};
        nutr.setValues(nutrLst);
        CmplSet food = new CmplSet("FOOD");
        String[] foodLst = {"BEEF", "CHK", "FISH", "HAM", "MCH",
                "MTL", "SPG", "TUR"};
        food.setValues(foodLst);
        CmplParameter costs = new CmplParameter("costs", food);
        Double[] costVec = {3.19, 2.59, 2.29, 2.89, 1.89, 1.99, 1.99, 2.49};
        costs.setValues(costVec);
        CmplParameter vitmin = new CmplParameter("vitMin", nutr);
        int [] vitminVec = { 700,700,700,700};
        vitmin.setValues(vitminVec);
        CmplParameter vitamin = new CmplParameter("vitamin", nutr, food);
        int[][] vitMat = {{60, 8, 8, 40, 15, 70, 25, 60},
                {20, 0, 10, 40, 35, 30, 50, 20},
                {10, 20, 15, 35, 15, 15, 25, 15},
                {15, 20, 10, 10, 15, 15, 15, 10}};
        vitamin.setValues(vitMat);
        model.setSets(nutr, food);
        model.setParameters(costs, vitmin, vitamin);
        model.connect("http://localhost:8008");
        model.solve();
        model.solutionReport();
        CmplSolution solution = model.solution();
        assertThat(solution.status()).isEqualTo("optimal");
        assertThat(solution.value()).isEqualTo(101.14);
        model.disconnect();
    }
}
