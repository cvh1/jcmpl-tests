package io.solvice.jcmpl.test;

import jCMPL.*;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TransportationTest {
    @Test
    public void solve() throws CmplException {
        Cmpl model = new Cmpl("target/test-classes/transportation.cmpl");
        CmplSet routes = new CmplSet("routes", 2);
        int[][] arcs = {{1, 1}, {1, 2}, {1, 4}, {2, 2}, {2, 3},
                {2, 4}, {3, 1}, {3, 3}};
        routes.setValues(arcs);
        CmplSet plants = new CmplSet("plants");
        plants.setValues(1, 3);
        CmplSet centers = new CmplSet("centers");
        centers.setValues(1, 1, 4);
        CmplParameter costs = new CmplParameter("c", routes);
        Integer[] costArr = {3, 2, 6, 5, 2, 3, 2, 4};
        costs.setValues(costArr);
        CmplParameter s = new CmplParameter("s", plants);
        int[] sList = {5000,6000,2500};
        s.setValues(sList);
        CmplParameter d = new CmplParameter("d", centers);
        int[] dArr = {6000, 4000, 2000, 2500};
        d.setValues(dArr);
        model.setSets(routes, plants, centers);
        model.setParameters(costs, s, d);
        model.setOutput(true);
        model.setOption("%display nonZeros");
        model.connect("http://localhost:8008");
        model.solve();
        model.solutionReport();
        CmplSolution solution = model.solution();
        assertThat(solution.status()).isEqualTo("optimal");
        assertThat(solution.value()).isEqualTo(36500.0);
        model.disconnect();
    }
}
