# End-to-end tests for jCMPL library and CMPLserver

These tests implements some use cases from CMPL user manual.

## Usage

```bash
docker-compose up
mvn test
```

